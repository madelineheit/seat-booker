package com.analystplatform.seatbooker;



public class MainApplication {

	private static void createSetupMessage() {
		  System.out.println("");
		  System.out.println("Welcome to Seat Booker");
		  System.out.println("");
		  System.out.println("Created by Maddy Heit - Lastest Release: 11/03/2019");
		  System.out.println("");
		  
		  }
	private static void loadWindow() {
	     //Create and set up the window.
		Reservation.setInitHashMap();
		Seat.initTheAuditorium();
		
		//Start the UI
		SeatBookingUI.main(null); 
			
			
	}
	
	public static void main(String... args) {
		
		
		// Creates message
		createSetupMessage();
		// Creates UI window of options
		loadWindow();
		//new SeatBookingDriver().run(new SeatBookingEngine());
		

	}

}
