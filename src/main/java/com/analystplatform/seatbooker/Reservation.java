package com.analystplatform.seatbooker;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Timer;

public class Reservation extends SeatReservation {

	public Reservation(int startSeatNumber, int rowNumber, int blockSize, String attendeeEmail) {
		super(startSeatNumber, rowNumber, blockSize, attendeeEmail);
		// TODO Auto-generated constructor stub
	}
	private static HashMap<Integer, SeatReservation> reservationValues; 
	
//	static Map <Integer, Object> reservationMap;
	
//	static int[] reservationNumbers;
	private static int startingSeatForGroup;
	private static int rowNumber;
	private static int blockSize;
	
	
	public static String generateTheBestSeatsByGuess(int partySize, String email) {
		//based on the party size and the number of available seats, try to grab the most front

			
		for(int i=1; i <= AppConsts.getVenueSizeRowLength(); i++) {
			
			if((partySize/i) <= AppConsts.getVenueSizeRowLength()) {
				//make a match
				
				int sizeOfBlockForRow = partySize/i;
				
				//counting the max number of availabe spots in a row
				int counterForNumberOfAvailableSpotsInARow = 0;
				int maxNumberOfAvailableSpotsInARow=0;
				int seatNumberGroupStartingSeat = 0;
				
				//check by row for party size
				for(int j=0;j<AppConsts.getVenueSizeRowLength(); j++) {
					seatNumberGroupStartingSeat = 0;
					for(int k=0; k<AppConsts.getVenueSizeColumnLength(); k++) {
						
						//if available seat
						if((Seat.getSingleSeatStatus(j, k)=="a")) {
							counterForNumberOfAvailableSpotsInARow++;
							if((k+1)<= AppConsts.getVenueSizeColumnLength()) {
								setHoldSeatForGroup(seatNumberGroupStartingSeat);
								setRowNumber(j);
								setBlockStore(sizeOfBlockForRow);
								return showTheSeatsSelected(seatNumberGroupStartingSeat, j, sizeOfBlockForRow);
							}
							
						} 
						//if the seat is currently being held or is already reserved
						else {
							//when the size of block needed for a row has been found
							//save the x position as the starting seat and
							if(sizeOfBlockForRow < k) {
								
								// if only one row of people and there are enough spots then set a temp hold for the attendee
								if((i==1) && (sizeOfBlockForRow<= maxNumberOfAvailableSpotsInARow)) {
									
									//need to go through loop and grab the exact row and column values
									setHoldSeatForGroup(seatNumberGroupStartingSeat);
									setRowNumber(j);
									setBlockStore(sizeOfBlockForRow);
									return showTheSeatsSelected(seatNumberGroupStartingSeat, j, sizeOfBlockForRow);
//									setAHoldForAttendee();
								
							}
							if(counterForNumberOfAvailableSpotsInARow > maxNumberOfAvailableSpotsInARow) {
								maxNumberOfAvailableSpotsInARow = counterForNumberOfAvailableSpotsInARow;
							}
							//reset the counter
							counterForNumberOfAvailableSpotsInARow = 0;
							seatNumberGroupStartingSeat = k;
						}
//						// if only one row of people and there are enough spots then set a temp hold for the attendee
//						if((i== 1)&& (j == 0) && ((partySize/i)< maxNumberOfAvailableSpotsInARow)) {
//							//need to go through loop and grab the exact row and column values
//							showTheSeatsSelected();
////							setAHoldForAttendee();
						}
					
						
					}
				}
			} 
		}
		System.out.println("seat algorithm");
		return "Error";
	}
	
	private static void setBlockStore(int sizeOfBlockForRow) {
		// TODO Auto-generated method stub
		Reservation.blockSize = sizeOfBlockForRow;
		
	}

	private static void setRowNumber(int j) {
		// TODO Auto-generated method stub
		Reservation.rowNumber = j;
	}

	private static void setHoldSeatForGroup(int seatNumberGroupStartingSeat) {
		// TODO Auto-generated method stub
		Reservation.startingSeatForGroup = seatNumberGroupStartingSeat;
	}

	private static int createAReservationNumber() {
		
		int n = 10000000 + new Random().nextInt(90000000);
		// 10000000 <= n <= 90000000 
		
		
		if(reservationValues.isEmpty()==false && reservationValues.get(n)!= null) {
			//then get a new number
			createAReservationNumber();
		}

		
		return n;
		
	}
	public static void moveTheHoldsBackToAvailable(int startingSeatForGroup, int RowNumber, int blockSize) {
		int seatCounter = startingSeatForGroup;
		for(int i=0; i< blockSize; i++) {
			Seat.setSingleSeatStatus(RowNumber, seatCounter, "a");
			seatCounter++;
		}
	}
	private static String showTheSeatsSelected(int startingSeatForGroup, int RowNumber, int blockSize) {
		
		int seatCounter = startingSeatForGroup;
		for(int i=0; i< blockSize; i++) {
			Seat.setSingleSeatStatus(RowNumber, seatCounter, "h");
			seatCounter++;
		}
		
		return Seat.buildTheHTMLForTheHeldSeats();
	}	
		
	
	
	private static void showTheSeatsSelectedForMultipleRows(int numOfRows) {
		
	}
	
	private static void setAHoldForAttendee() {
		//if the selection is good, then start a timer for 60 seconds, then set the seats to held 
		//if the selection is confirmed (okayed), within 60 seconds and email is given, then give a reservation number and set the seats to reserved (=r)
		
		// setTheHeldReservationToAvailable or setAReservationforAttendee();
		
		
		
			Timer timer = new Timer();
			timer.schedule(new DisplayCountdown(),0,1000);
			System.out.print(timer);
		
		
		
		
	}
	
	public static int createAMappedReservation(int startingSeatForGroup, int rowNumber, int blockSize, String attendeeEmail) {
//		System.out.println(reservationValues);
		
		SeatReservation reservation = new SeatReservation(startingSeatForGroup, rowNumber, blockSize, attendeeEmail);	
		int resNum = createAReservationNumber();
		reservationValues.put(resNum,reservation);
		
		
		int seatCounter = startingSeatForGroup;
		for(int i=0; i<= blockSize; i++) {
			Seat.setSingleSeatStatus(rowNumber, seatCounter, "r");
			seatCounter++;
		}
		
		
		
		
		return resNum;
		//public Reservation(int startSeatNumber, int rowNumber, int blockSize)
	}
	
	
	public static void setInitHashMap() {
		//if the selection is confirmed (okayed), within 60 seconds and email is given, then give a reservation number and set the seats to reserved (=r)
		
		
		//check if time is 60 seconds
		
		reservationValues = new HashMap();

		
		
	}
	private static void setTheHeldReservationToAvailable() {
		//if the selection is cofrimed (okayed), within 60 seconds and email is given, then give a reservation number and set the seats to reserved (=r)
		
	}
	
	
	public static boolean lookupReservationConfirmationForAttendee(int confirmationID, String email) {
		
		
		boolean isKeyPresent = false;
		boolean doesTheConfirmationNumberMatchTheEmail = false;
		
		Iterator<Entry<Integer, SeatReservation>> iterator = reservationValues.entrySet().iterator();
		
		while (iterator.hasNext()) {
		Entry<Integer, SeatReservation> entry = iterator.next();
		
		  if(confirmationID == entry.getKey()) {
			isKeyPresent = true;
			System.out.println("The confirmation key exists in the map");
			SeatReservation map = reservationValues.get(confirmationID);
			String emailValueFromHashMap = map.getAttendeeEmail();
			
			
			//Must use .equals and not compare variables 
			if(emailValueFromHashMap.equals(email) == true) {
				doesTheConfirmationNumberMatchTheEmail = true;
				System.out.println("The emails both match");
			}
				
			
		}//looking for a key to match the hashmap

		} //end of while
		
		//will return true if only the two requirements are met
		return doesTheConfirmationNumberMatchTheEmail;
		
		
		
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static int getHoldSeatForGroup() {
		// TODO Auto-generated method stub
		return Reservation.startingSeatForGroup;
	}

	public static int getBlockSize() {
		// TODO Auto-generated method stub
		return Reservation.blockSize;
	}

	public static int getHoldRowNumber() {
		// TODO Auto-generated method stub
		return Reservation.rowNumber;
	}

	public static String getSeatValueData(Integer resNum) {
		// TODO Auto-generated method stub
		SeatReservation map = reservationValues.get(resNum);
		
		String tickSeatingString = "Row "+(char)('@'+map.getRowNumber()+1) + " - Seats: ";
		int seatCounter = map.getStartingSeatNumber();
		for(int i=(map.getStartingSeatNumber()+1); i<= (map.getsBlockSize()+map.getStartingSeatNumber()); i++) {
			tickSeatingString += (" "+ i + " ");
			seatCounter++;
		}
		
		return tickSeatingString;
	}

	

}
