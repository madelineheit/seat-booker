package com.analystplatform.seatbooker;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;
import java.util.Timer;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;




public class SeatBookingUI {

//	private static JFrame frame= new JFrame(); 
    
	
	public static void createFancyWindow() {
		// Create frame with title Registration Demo
        JFrame frame= new JFrame(); 
        frame.setTitle("Seat Booker Demo");
         
               // Panel to define the layout. We are using GridBagLayout
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
 
        JPanel headingPanel = new JPanel();
        JLabel headingLabel = new JLabel("Please make a selection from the following options.");
        headingPanel.add(headingLabel);
         
               // Panel to define the layout. We are using GridBagLayout
        JPanel panel = new JPanel(new GridBagLayout());
        // Constraints for the layout
        GridBagConstraints constr = new GridBagConstraints();
        constr.insets = new Insets(5, 5, 5, 5);     
        constr.anchor = GridBagConstraints.WEST;
 
        // Set the initial grid values to 0,0
        constr.gridx=0;
        constr.gridy=0;
  

         JButton buttonSeatCount = new JButton("View Available Seat Count");
		 JButton buttonExistingReservation = new JButton("Get Reservation Infomation");
		 JButton buttonNewReservation = new JButton("Create a New Reservation");
//		 JButton buttonPrintExistingReservation = new JButton("Print Existing Reservation");
		 
		 buttonSeatCount.setAlignmentX(Component.BOTTOM_ALIGNMENT);
		 buttonExistingReservation.setAlignmentX(Component.BOTTOM_ALIGNMENT);
		 buttonNewReservation.setAlignmentX(Component.BOTTOM_ALIGNMENT);
//		 buttonPrintExistingReservation.setAlignmentX(Component.BOTTOM_ALIGNMENT);
		 
		 
		 buttonSeatCount.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Selected: buttonSeatCount");

				panel.removeAll();
				panel.revalidate();
				panel.repaint();
				headingPanel.removeAll();
				headingPanel.revalidate();
				headingPanel.repaint();
				
				// heading
				JLabel headingLabel = new JLabel("Below shows the current booking of "+Seat.getNumberOfAvailableSeats() +" available seats");
				headingPanel.add(headingLabel);
				headingPanel.revalidate();
				headingPanel.repaint();
				
				//Stage output
				JLabel bodyLabel = new JLabel(Seat.getHTMLAuditoriumStatus());
				
				
				//Back button
				JButton backButton = new JButton("Main Menu");
				
				backButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						SeatBookingUI.createFancyWindow();
					}
				});
				
				JPanel buttonPane = new JPanel();
				buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));
				
				buttonPane.add(bodyLabel);
//				buttonPane.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
				buttonPane.add(Box.createVerticalGlue());
				buttonPane.add(Box.createRigidArea(new Dimension(0,30)));
				buttonPane.add(backButton);
				
				panel.add(buttonPane);
				

				panel.revalidate();
				panel.repaint();
				
				
				
			
			}
			 
		 });
		 buttonExistingReservation.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					System.out.println("Selected: buttonExistingReservation");
//					ExisitingReservationSelectionUI.main(null);
					
					
					// heading
					JLabel headingLabel = new JLabel("Please enter in the appropiate information.");
					headingPanel.add(headingLabel);
					headingPanel.revalidate();
					headingPanel.repaint();
					
			
					
					GridBagConstraints gbc = new GridBagConstraints();

					gbc.fill = GridBagConstraints.HORIZONTAL;
					
					
					panel.removeAll();
					panel.revalidate();
					panel.repaint();
					
					JLabel emailLabel = new JLabel();
					emailLabel.setText("Enter your email: ");				
					gbc.gridx = 0;
					gbc.gridy = 0;
					panel.add(emailLabel, gbc);
					
					JTextField emailTextfield = new JTextField("email@address.com");
					gbc.gridy = 1;
					gbc.gridx = 0;
					panel.add(emailTextfield, gbc);
					
					JLabel emptyContent0 = new JLabel();
					emptyContent0.setPreferredSize(new Dimension(100,25));
					gbc.gridy = 2;
					gbc.gridx = 0;
					panel.add(emptyContent0, gbc);
					
					JLabel confirmationNumberLabel = new JLabel();
					confirmationNumberLabel.setText("Enter confirmation number:");
					
					gbc.gridx = 0;
					gbc.gridy = 3;
					panel.add(confirmationNumberLabel, gbc);
					
					JTextField confirmationNumberTextfield = new JTextField("");
					gbc.gridy = 4;
					gbc.gridx = 0;
					panel.add(confirmationNumberTextfield, gbc);
					
					JLabel emptyContent = new JLabel();
					emptyContent.setPreferredSize(new Dimension(100,25));
					gbc.gridy = 5;
					gbc.gridx = 0;
					panel.add(emptyContent, gbc);
					
					JButton mm = new JButton("Main Menu");

					gbc.gridy = 8;
					gbc.gridx = 0;
					gbc.gridwidth = 2;

					panel.add(mm, gbc);
					
					
					
					JLabel emptyContent1 = new JLabel();
					emptyContent1.setPreferredSize(new Dimension(100,25));
					gbc.gridy = 7;
					gbc.gridx = 0;
					panel.add(emptyContent1, gbc);
					
					JButton submitButton = new JButton("Submit");

					gbc.gridy = 6;
					gbc.gridx = 0;
					gbc.gridwidth = 2;

					panel.add(submitButton, gbc);
					
					
					mm.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							System.out.println("User clicked Main Menu Selection From the new reservation selection");
							SeatBookingUI.createFancyWindow();
						}
					});
					submitButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							System.out.println("User clicked sumbit and now the lookup will begin");
							int conNum = Integer.parseInt(confirmationNumberTextfield.getText());
							
							if(Reservation.lookupReservationConfirmationForAttendee(conNum, emailTextfield.getText()) == true) {
								// keep 
								String seatData = Reservation.getSeatValueData(conNum);
//								String confirmationNum = String.valueOf(conNum);
								panel.removeAll();
								panel.revalidate();
								panel.repaint();
								
								JLabel emailLabel = new JLabel("The email linked to your reservation:");	
								gbc.gridx = 0;
								gbc.gridy = 0;
								panel.add(emailLabel, gbc);
								
								JLabel emailTextfield2 = new JLabel(emailTextfield.getText());
								gbc.gridy = 1;
								gbc.gridx = 0;
								panel.add(emailTextfield2, gbc);
								
								JLabel confirmationNumberLabel = new JLabel();
								confirmationNumberLabel.setText("Your reservation confirmation number:");
								
								gbc.gridx = 0;
								gbc.gridy = 2;
								panel.add(confirmationNumberLabel, gbc);
								
								JLabel confirmationNumberTextfield = new JLabel(String.valueOf(conNum));
								gbc.gridy = 3;
								gbc.gridx = 0;
								panel.add(confirmationNumberTextfield, gbc);
								
								JLabel seatLabel = new JLabel("Your seat Information:");
								gbc.gridx = 0;
								gbc.gridy = 4;
								panel.add(seatLabel, gbc);
								
								JLabel seatTestLabel = new JLabel(seatData);
								gbc.gridy = 5;
								gbc.gridx = 0;
								panel.add(seatTestLabel, gbc);
								
								JLabel emptyContent = new JLabel();
								emptyContent.setPreferredSize(new Dimension(100,25));
								gbc.gridy = 6;
								gbc.gridx = 0;
								
								panel.add(emptyContent, gbc);
								JLabel emptyContent0 = new JLabel();
								emptyContent0.setPreferredSize(new Dimension(100,25));
								gbc.gridy = 8;
								gbc.gridx = 0;
								panel.add(emptyContent0, gbc);
								
								JButton printButton = new JButton("Print Page");

								gbc.gridy = 7;
								gbc.gridx = 0;
								gbc.gridwidth = 2;

								panel.add(printButton, gbc);
								
								
								
								JLabel emptyContent1 = new JLabel();
								emptyContent1.setPreferredSize(new Dimension(100,25));
								gbc.gridy = 9;
								gbc.gridx = 0;
								panel.add(emptyContent1, gbc);
								
								JButton mm = new JButton("Main Menu");

								gbc.gridy = 10;
								gbc.gridx = 0;
								gbc.gridwidth = 2;

								panel.add(mm, gbc);
								
								
								
								mm.addActionListener(new ActionListener() {
									
									@Override
									public void actionPerformed(ActionEvent e) {
										// TODO Auto-generated method stub
										createFancyWindow();
										
									}
								});
								
								printButton.addActionListener(new ActionListener() {
									
									@Override
									public void actionPerformed(ActionEvent e) {
										// TODO Auto-generated method stub
										PrinterJob pj = PrinterJob.getPrinterJob();
										pj.setJobName(" Seat-Booker Printer Job");
										
										
										pj.setPrintable(new Printable() {

											@Override
											public int print(Graphics pg, PageFormat pf, int pageNum) {
												if(pageNum> 0)
													return Printable.NO_SUCH_PAGE;
												Graphics2D g2 = (Graphics2D)pg;
												g2.translate(pf.getImageableX(), pf.getImageableY());
												panel.paintAll(g2);

											return Printable.PAGE_EXISTS;
											
											
											
											}
													
											
										});
										if(pj.printDialog() == false)
											return;
										try {
											pj.print();
										}
										catch(PrinterException xcp) {
											xcp.printStackTrace(System.err);
										}
										
										
										
									}//end of action performed
									
								}); //end of print page
								
								
							}
							else {
								JOptionPane.showMessageDialog(panel, "The given value doesn't match our records. Try again.");
							}
							
							
							
							
//							SeatBookingUI.createFancyWindow();
						}
					});
					
					
					
					//end for button information
					
					
				}
				 
			 }); //finished lookupreservation
		 
		 // to create new reservation
		 buttonNewReservation.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					System.out.println("Selected: buttonNewReservation");
					
					panel.removeAll();
					panel.revalidate();
					panel.repaint();
					headingPanel.removeAll();
					headingPanel.revalidate();
					headingPanel.repaint();
					
					
					// heading
					JLabel headingLabel = new JLabel("Please enter in the appropiate information.");
					headingPanel.add(headingLabel);
					headingPanel.revalidate();
					headingPanel.repaint();
					
			
					
					GridBagConstraints gbc = new GridBagConstraints();

					gbc.fill = GridBagConstraints.HORIZONTAL;
					
					
					
					
					JLabel emailLabel = new JLabel();
					emailLabel.setText("Enter your email: ");				
					gbc.gridx = 0;
					gbc.gridy = 0;
					panel.add(emailLabel, gbc);
					
					JTextField emailTextfield = new JTextField("email@address.com");
					gbc.gridy = 1;
					gbc.gridx = 0;
					panel.add(emailTextfield, gbc);
					
					JLabel emptyContent0 = new JLabel();
					emptyContent0.setPreferredSize(new Dimension(100,25));
					gbc.gridy = 2;
					gbc.gridx = 0;
					panel.add(emptyContent0, gbc);
					
					JLabel partySizeLabel = new JLabel();
					partySizeLabel.setText("Enter party size: ");
					
					gbc.gridx = 0;
					gbc.gridy = 3;
					panel.add(partySizeLabel, gbc);
					
					JTextField partySizeTextfield = new JTextField("1");
					gbc.gridy = 4;
					gbc.gridx = 0;
					panel.add(partySizeTextfield, gbc);
					
					JLabel emptyContent = new JLabel();
					emptyContent.setPreferredSize(new Dimension(100,25));
					gbc.gridy = 5;
					gbc.gridx = 0;
					panel.add(emptyContent, gbc);
					
					JButton mm = new JButton("Main Menu");

					gbc.gridy = 8;
					gbc.gridx = 0;
					gbc.gridwidth = 2;

					panel.add(mm, gbc);
					
					
					
					JLabel emptyContent1 = new JLabel();
					emptyContent1.setPreferredSize(new Dimension(100,25));
					gbc.gridy = 7;
					gbc.gridx = 0;
					panel.add(emptyContent1, gbc);
					
					JButton submitButton = new JButton("Submit");

					gbc.gridy = 6;
					gbc.gridx = 0;
					gbc.gridwidth = 2;

					panel.add(submitButton, gbc);
					
					
					mm.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							System.out.println("User clicked Main Menu Selection From the new reservation selection");
							SeatBookingUI.createFancyWindow();
						}
					});
					
					submitButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							System.out.println("User clicked submit, need to grab email and party size");
							System.out.println("Email provided: "+ emailTextfield.getText());
							System.out.println("Party size provided: "+ partySizeTextfield.getText());
							
							try {
								
								//checking for the party size number to be a number, to be larger than 0 and less than the number of available seats
								int partySize = Integer.parseInt(partySizeTextfield.getText());	
								if(partySize > Seat.getNumberOfAvailableSeats()) {
									JOptionPane.showMessageDialog(frame, "The party size value that was inputted is unfortunatly too big and cannot be accomidated for our venue. Sorry for the inconvience, please enter in a smaller party size and try again.", "Party Size Error", JOptionPane.ERROR_MESSAGE);
								}
								else if(partySize < 1) {
									JOptionPane.showMessageDialog(frame, "The party size value that was inputted is unfortunatly too small. Please enter in a different value and try again.", "Party Size Error", JOptionPane.ERROR_MESSAGE);
								}
								// checking to see if the email address is valid
								if(Attendee.isTheEmailAddressValid(emailTextfield.getText()) == true){
									
									
									
									
									String valueForPane = Reservation.generateTheBestSeatsByGuess(partySize, emailTextfield.getText());
									

									
									JLabel seatsToDraw = new JLabel(valueForPane, JLabel.CENTER);
									seatsToDraw.setPreferredSize(new Dimension(800,800));
									
									headingPanel.remove(headingLabel);
									JLabel headingLabel = new JLabel("Please make a selection from the buttons below.");
									headingPanel.add(headingLabel);
									headingPanel.revalidate();
									headingPanel.repaint();
									
									
									
									
									panel.removeAll();
									panel.revalidate();
									panel.repaint();

									panel.add(seatsToDraw);
									
									
									JLabel countDownTimerOfOneMinute = new JLabel("You have 60 seconds to hold your reservation");
									long startTimeForCounter= -1;
									long duration = 700;
									Timer timerToUse = null;
									
//									timerToUse = new Timer(600, new ActionListener() {
//										
//										@Override
//										public void actionPerformed(ActionEvent e) {
//											// TODO Auto-generated method stub
//											if (startTimeForCounter < 0) {
//												startTimeForCounter = System.currentTimeMillis();
//											}
//											long now = System.currentTimeMillis();
//													long clockTime = now-startTimeForCounter;
//											if(clockTime >= duration) {
//												clockTime = duration;
//												timerToUse.stop();
//												SeatBookingUI.createFancyWindow();
//												
//											}
//											SimpleDateFormat df = new SimpleDateFormat("mm:ss:SSS");
//											countDownTimerOfOneMinute.setText("You have " +df.format(duration - clockTime)+" time left to hold your reservation");
//											System.out.println("You have " +df.format(duration - clockTime)+" time left to hold your reservation");
//										}
//									});
									
									
									
									
//									Timer simpleTimer = new Timer(100, new ActionListener() {
//										
//										@Override
//										public void actionPerformed(ActionEvent e) {
//											// TODO Auto-generated method stub
//											countDownTimerOfOneMinute.setText("You have " +" time left to hold your reservation");
//											System.out.println("You have " +" time left to hold your reservation");
//										}
//									}); 
//									simpleTimer.start();
									
									
									
									
									
									
									gbc.gridx = 0;
									gbc.gridy = 1;
									panel.add(countDownTimerOfOneMinute, gbc);
									
									JButton reserveSeats = new JButton("Reserve Seats");
									gbc.gridy = 2;
									gbc.gridx = 0;
									panel.add(reserveSeats, gbc);
									
									JLabel emptyContent1 = new JLabel();
									emptyContent1.setPreferredSize(new Dimension(100,25));
									gbc.gridy = 3;
									gbc.gridx = 0;
									panel.add(emptyContent1, gbc);
									
//									JButton selectSeats = new JButton("Select My Own Seats");
//									emptyContent0.setPreferredSize(new Dimension(100,25));
									gbc.gridy = 4;
									gbc.gridx = 0;
//									panel.add(selectSeats, gbc);
									
									JLabel emptyContent2 = new JLabel();
									emptyContent2.setPreferredSize(new Dimension(100,25));
									gbc.gridy = 5;
									gbc.gridx = 0;
									panel.add(emptyContent2, gbc);
									
									JButton mainMenu = new JButton("Main Menu");
									gbc.gridx = 0;
									gbc.gridy = 6;
									panel.add(mainMenu, gbc);
									
									reserveSeats.addActionListener(new ActionListener() {

										@Override
										public void actionPerformed(ActionEvent e) {
											// TODO Auto-generated method stub
											//need to call reservation and update seats
											int reservationNum = Reservation.createAMappedReservation(Reservation.getHoldSeatForGroup(), Reservation.getHoldRowNumber(), Reservation.getBlockSize(), emailTextfield.getText());
											
											panel.removeAll();
											panel.revalidate();
											panel.repaint();
//											panel.setBackground(Color.green);
											panel.add(seatsToDraw);
											
											 
//											gbc.gridwidth = 3;
											JLabel showTicketEmail = new JLabel("Your Confirmation Email: "+ emailTextfield.getText());
											gbc.gridx = 0;
											gbc.gridy = 1;
											panel.add(showTicketEmail, gbc);
											
											JLabel showReservatioNum = new JLabel("Your Ticket Confirmation Number: "+ reservationNum);
											gbc.gridy = 2;
											gbc.gridx = 0;
											panel.add(showReservatioNum, gbc);
											
											JLabel emptyContent1 = new JLabel("Seats: "+Reservation.getSeatValueData(reservationNum));
											emptyContent1.setPreferredSize(new Dimension(100,25));
											gbc.gridy = 3;
											gbc.gridx = 0;
											panel.add(emptyContent1, gbc);
											
											JButton printPage = new JButton("Print");
//											emptyContent0.setPreferredSize(new Dimension(100,25));
											gbc.gridy = 4;
											gbc.gridx = 0;
											panel.add(printPage, gbc);
											
											JLabel emptyContent2 = new JLabel();
//											emptyContent0.setPreferredSize(new Dimension(100,25));
											gbc.gridy = 5;
											gbc.gridx = 0;
											panel.add(emptyContent2, gbc);
											
											JButton mainMenu = new JButton("Main Menu");
//											emptyContent0.setPreferredSize(new Dimension(100,25));
											gbc.gridy = 6;
											gbc.gridx = 0;
											panel.add(mainMenu, gbc);
											
											
											mainMenu.addActionListener(new ActionListener() {
												
												@Override
												public void actionPerformed(ActionEvent e) {
													// TODO Auto-generated method stub
													SeatBookingUI.createFancyWindow();
												}
												
											});	
											printPage.addActionListener(new ActionListener() {
												
												@Override
												public void actionPerformed(ActionEvent e) {
													// TODO Auto-generated method stub
													PrinterJob pj = PrinterJob.getPrinterJob();
													pj.setJobName(" Seat-Booker Printer Job");
													
													
													pj.setPrintable(new Printable() {

														@Override
														public int print(Graphics pg, PageFormat pf, int pageNum) {
															if(pageNum> 0)
																return Printable.NO_SUCH_PAGE;
															Graphics2D g2 = (Graphics2D)pg;
															g2.translate(pf.getImageableX(), pf.getImageableY());
															panel.paintAll(g2);

														return Printable.PAGE_EXISTS;
														
														
														
														}
																
														
													});
													if(pj.printDialog() == false)
														return;
													try {
														pj.print();
													}
													catch(PrinterException xcp) {
														xcp.printStackTrace(System.err);
													}
													
													
													
												}//end of action performed
												
											}); //end of print page
											
											
											
											
											
										}
										
									});
//									selectSeats.addActionListener(new ActionListener() {
//
//										@Override
//										public void actionPerformed(ActionEvent e) {
//											// TODO Auto-generated method stub
//											SeatBookingUI.createFancyWindow();
//										}
//										
//									});
									mainMenu.addActionListener(new ActionListener() {

										@Override
										public void actionPerformed(ActionEvent e) {
											// TODO Auto-generated method stub
											
											//set all holds to available if going back to main menu
											Reservation.moveTheHoldsBackToAvailable(Reservation.getHoldSeatForGroup(), Reservation.getHoldRowNumber(), Reservation.getBlockSize());
											
											SeatBookingUI.createFancyWindow();
										}
										
									});
									
									
								} else {
									//when the email address is not valid
									JOptionPane.showMessageDialog(frame, "The email address provided is unfortunatly not valid. Please enter in a different value and try again.", "Email Error", JOptionPane.ERROR_MESSAGE);
								}

							} //catch when the party size is not a number
							catch(Exception e1){
								JOptionPane.showMessageDialog(frame, "The party size value that was inputted is unfortunatly not a valid number. Please enter in a different value and try again.", "Party Size Error", JOptionPane.ERROR_MESSAGE);
								
							}
							
						}
					});

					
					panel.setVisible(true);
					panel.revalidate();
					panel.repaint();
					
					

					
					
					
				}
				 
			 });
		 
         

        constr.gridx=1;
        panel.add(buttonSeatCount, constr);
        constr.gridx=0; constr.gridy=1;
         

        constr.gridx=1;
        panel.add(buttonNewReservation, constr);
        constr.gridx=0; constr.gridy=2;
         

        constr.gridx=1;
        panel.add(buttonExistingReservation, constr);
        constr.gridy=3;
        

      constr.gridx=1;
//      panel.add(buttonPrintExistingReservation, constr);
      constr.gridy=4;
         
        constr.gridwidth = 2;
        constr.anchor = GridBagConstraints.CENTER;
  

  
        mainPanel.add(headingPanel);
        mainPanel.add(panel);
 
        // Add panel to frame
        frame.add(mainPanel);
        frame.pack();
        frame.setSize(AppConsts.getWindowSizeOfX(), AppConsts.getWindowSizeOfY());
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
	}
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		createFancyWindow();
		
		
		
	}

}
