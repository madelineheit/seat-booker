package com.analystplatform.seatbooker;

public class AppConsts {

	private static int windowSizeOfX = 1000;
	private static int windowSizeOfY = 1000;
	private static int venueRowLength = 26;
	private static int venueColumnLength = 26;

	
	public static int getWindowSizeOfX() {
		return windowSizeOfX;
	}
	public static int getWindowSizeOfY() {
		return windowSizeOfY;
	}
	public static int getVenueSizeRowLength() {
		return venueRowLength;
	}
	public static int getVenueSizeColumnLength() {
		return venueColumnLength;
	}
	
	
	


}
