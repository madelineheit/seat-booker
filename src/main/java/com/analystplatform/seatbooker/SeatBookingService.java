package com.analystplatform.seatbooker;



//* <pre>
//* // Returns all the seats 
//* public Seat[][] getSeats();
//* 
//* // Returns the number of rows in the venue
//* public int getNumberOfRows();
//* 
//* // Returns the number of columns in the venue
//* public int getNumberOfColumns();
//* 
//* // Get all the Seats being held, mapped to the IDs of their holds
//* public Map<Integer, SeatHold> getSeatHolds();
//* 
//* // Removes all the seat holds of the given IDs
//* public void removeAllSeatHolds(List<Integer> seatHoldIds);
//* 
//* // Holds a list of Seats for the given customer email address. Creates IDs and updates the in-memory data model with
//* // the held seats
//* public SeatHold newSeatHold(String customerEmail, List<Seat> seatsToHold);
//* 
//* // Returns a seat hold for the given ID. Returns null if one wasn't found
//* public SeatHold getSeatHold(int id);
//* 
//* // Removes a seat hold from the database of seat holds
//* public void removeSeatHold(int id);
//* 
//* // Returns any expired seats into the pool of available seats.
//* public void reclaimExpiredHolds();
//* 
//* // Returns the given confirmation for a valid id, null otherwise
//* public ReservationConfirmation getReservationConfirmation(String confirmationNumber);
//* 
//* // Saves the given confirmation
//* public void saveReservationConfirmation(ReservationConfirmation confirmation);
//* 
//* // Returns all reservation confirmations as a list
//* public List<ReservationConfirmation> getReservationConfirmations();
//* </pre>
//* 
//*/



public class SeatBookingService {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
