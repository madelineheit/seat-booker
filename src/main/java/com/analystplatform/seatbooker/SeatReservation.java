package com.analystplatform.seatbooker;

public class SeatReservation {
	
	int startSeatNumber;
	int rowNumber;
	int blockSize;
	String attendeeEmail;
	
	
	public SeatReservation (int startSeatNumber, int rowNumber, int blockSize, String attendeeEmail) {
		this.startSeatNumber= startSeatNumber;
		this.rowNumber= rowNumber;
		this.blockSize= blockSize;
		this.attendeeEmail = attendeeEmail;
	}
	
	public int getStartingSeatNumber() {
		return startSeatNumber;
	}
	public int getRowNumber() {
		return rowNumber;
	}
	public int getsBlockSize() {
		return blockSize;
	}
	public String getAttendeeEmail() {
		return attendeeEmail;
	}
	
	public static void main(String[] args) {
//		SeatReservation reservation = new Reservation();
	}

}
