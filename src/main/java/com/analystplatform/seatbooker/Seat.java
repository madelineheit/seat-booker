package com.analystplatform.seatbooker;

public class Seat {


	private String seatStatus;
	private static String[] auditorium[];
	
	public Seat(String seatStatus) {
		this.seatStatus = seatStatus;
	}
	
	public static String getSingleSeatStatus(int row, int column) {
		if(auditorium[row][column]!="a" && auditorium[row][column]!="h" && auditorium[row][column]!="r"  ){
			System.out.println("Error: located in Seat Class in getSeatStatusFunction - value : '" + auditorium[row][column]+ "' row: "+ row+ ",  column: "+ column);
		}			
			return auditorium[row][column];				
	}
	
	public static int getNumberOfAvailableSeats() {
		int counterOfAvailableSeats = 0;
		
		for(int i=0; i<(AppConsts.getVenueSizeRowLength());i++) {
			for(int j=0; j<(AppConsts.getVenueSizeColumnLength());j++) {
				if(auditorium[i][j]=="a") {
					counterOfAvailableSeats++;
				}
			}
		}
		
		return counterOfAvailableSeats;				
	}
	
	public static String getAuditoriumStatus() {
		
		String auditoriumStatus = "";
		for(int i=0; i<(AppConsts.getVenueSizeRowLength());i++) {
			for(int j=0; j<(AppConsts.getVenueSizeColumnLength());j++) {
				auditoriumStatus+= auditorium[i][j] +" ";
			}
			auditoriumStatus+="\n";
		}
		
		
		
		return auditoriumStatus;
		
	}

	
	
public static String buildTheHTMLForTheHeldSeats	() {
	String auditoriumStatus = "<html><center>  Stage<br><br>";
	for(int i=0; i<(AppConsts.getVenueSizeRowLength());i++) {
		auditoriumStatus += "Row "+(char)('@'+i+1) + ": ";
		for(int j=0; j<(AppConsts.getVenueSizeColumnLength());j++) {
			if(auditorium[i][j]=="a") {
				auditoriumStatus+= "<font color='black'>X </font>";
			}else if(auditorium[i][j]=="r") {
				auditoriumStatus+= "<font color='black'>X </font>";
			} else {
				auditoriumStatus+= "<font color='blue'>X </font>";
			}
				
			
			
		}
		auditoriumStatus+="<br>";
	}
	auditoriumStatus+= "</center></html>";
	
	
	return auditoriumStatus;
}
	
	
public static String getHTMLAuditoriumStatus() {

			
		String auditoriumStatus = "<html><center>  Stage<br>";
		for(int i=0; i<(AppConsts.getVenueSizeRowLength());i++) {
			auditoriumStatus += "Row "+(char)('@'+i+1) + ": ";
			for(int j=0; j<(AppConsts.getVenueSizeColumnLength());j++) {
				if(auditorium[i][j]=="a") {
					auditoriumStatus+= "<font color='green'>a </font>";
				}else if(auditorium[i][j]=="r") {
					auditoriumStatus+= "<font color='red'>r </font>";
				} else {
					auditoriumStatus+= "<font color='blue'>h </font>";
				}
					
				
				
			}
			auditoriumStatus+="<br>";
		}
		auditoriumStatus+= "</center></html>";
		
		
		return auditoriumStatus;
		
	}
	
	public static void setSingleSeatStatus(int row, int column, String status) {
		auditorium[row][column]=status;
		System.out.println("Status Update of "+ status + " to: "+ row + ", "+ column);
	}
	
	
	public static void initTheAuditorium() {
		auditorium = new String[AppConsts.getVenueSizeRowLength()][AppConsts.getVenueSizeColumnLength()];
		for (int i=0; i<(AppConsts.getVenueSizeRowLength());i++) {
			for(int j=0; j<(AppConsts.getVenueSizeColumnLength());j++) {
				auditorium[i][j] = "a";
			}
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
